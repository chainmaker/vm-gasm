module chainmaker.org/chainmaker/vm-gasm/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.5
	chainmaker.org/chainmaker/logger/v2 v2.3.4
	chainmaker.org/chainmaker/pb-go/v2 v2.3.6
	chainmaker.org/chainmaker/protocol/v2 v2.3.6
	chainmaker.org/chainmaker/store/v2 v2.3.6
	chainmaker.org/chainmaker/vm/v2 v2.3.6
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
	github.com/stretchr/testify v1.8.2
)
